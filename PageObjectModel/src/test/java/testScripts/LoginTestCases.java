package testScripts;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pageObjects.LoginPage;


public class LoginTestCases 
{
	WebDriver driver ;
	LoginPage lPage;
	Properties prop;
	
	@BeforeTest
	@Parameters("browserName")
	public void setup(String browserName) throws FileNotFoundException, IOException
	{
		prop = new Properties();
		prop.load(new FileInputStream("credentials.properties"));
	
		if(browserName.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		else
		{
			System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/V1/index.php");
	}
	
	@Test(priority = 1)
	public void testLogin()
	{
		String uid = prop.getProperty("uid");
		String pwd = prop.getProperty("pwd");
		
		lPage = new LoginPage(driver);
		
		lPage.login(uid, pwd);
		
		String title = lPage.getPageTitle(driver);
		
		assertEquals(title, "GTPL Bank Manager HomePage");
	}
	
	@Test(priority = 2)
	public void logout() throws InterruptedException
	{
		lPage.logout(driver);
		
		String title = lPage.getPageTitle(driver);
		
		assertEquals(title, "GTPL Bank Home Page");
		
		Thread.sleep(3000);
	}
	
	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
	}
}
