package pageObjects;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage 
{
	WebDriver ldriver;
	
	@FindBy(name="uid")
	WebElement txtuserid;
	
	@FindBy(name="password")
	WebElement txtpwd;
	
	@FindBy(name="btnLogin")
	WebElement btnLogin;
	
	@FindBy(linkText = "Log out")
	WebElement btnLogout;
	
	public LoginPage(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(ldriver, this);
	}
	
	
	public void login(String uid,String pwd)
	{
		txtuserid.clear();
		txtuserid.sendKeys(uid);
		
		txtpwd.clear();
		txtpwd.sendKeys(pwd);
		
		btnLogin.click();
	}
	
	public void logout(WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor jexec = (JavascriptExecutor) driver;
		jexec.executeScript("arguments[0].scrollIntoView(true);", btnLogout);
		
		//btnLogout.click();
		jexec.executeScript("arguments[0].click();", btnLogout);

		Thread.sleep(2000);
		
		String alertMessage = driver.switchTo().alert().getText();
		
		driver.switchTo().alert().accept();
		
		assertEquals(alertMessage, "You Have Succesfully Logged Out!!");
		
		Thread.sleep(2000);

	}
	
	public String getPageTitle(WebDriver driver)
	{
		String title = driver.getTitle();
		
		return title;
	}
	}
